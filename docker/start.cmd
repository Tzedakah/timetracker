@echo off

IF [%1]==[--release] (
  SET additional-arguments=
  SET command=sh release.sh
) ELSE IF [%1] ==[--node-environment] (
  SET additional-arguments=-it -p 127.0.0.1:9926:9926
  SET command=/bin/bash
) ELSE (
  SET additional-arguments=-p 127.0.0.1:9926:9926
  SET command=sh debug.sh
)

docker run --name timetracker --rm -v %~dp0..:/timetracker -w /timetracker -p 127.0.0.1:8080:8080 %additional-arguments% node %command%

pause
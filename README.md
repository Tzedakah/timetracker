# TimeTracker


## Setup
* Make sure you have Docker installed and running
* `TIME_TRACKER_REPO` can be set as an environment variable to the root directory of your local repository of the TimeTracker
* Otherwise replace `%TIME_TRACKER_REPO%` in the following with the correct file path

## Run TimeTracker
* Start TimeTracker using `%TIME_TRACKER_REPO%\docker\start.cmd` by running 
  ```
  cmd /c "$Env:TIME_TRACKER_REPO\docker\start.cmd" [--release]
  ```
  in your PowerShell.
* When the `release` flag is omitted, then the TimeTracker is started in a debug mode in which one can connect a debugger and in which hot reloading is enabled. 
  Otherwise TimeTracker is started in mode designed for production. 
  In the latter mode TimeTracker is first build and then the result is run with Node.
* To load the test page, enter in your browser url bar:
  `localhost:8080`.
* Stop TimeTracker using `%TIME_TRACKER_REPO%\docker\stop.cmd` by either double clicking the file or running 
  ```
  cmd /c "$Env:TIME_TRACKER_REPO\docker\stop.cmd" 
  ```
  in your PowerShell.

## Run a shell in TimeTrackers environment
* Start the shell using `%TIME_TRACKER_REPO%\docker\start.cmd` by running 
  ```
  cmd /c "$Env:TIME_TRACKER_REPO\docker\start.cmd" --node-environment
  ```
  in your PowerShell.
* Close the shell using `%TIME_TRACKER_REPO%\docker\stop.cmd` by either double clicking the file or running 
  ```
  cmd /c "$Env:TIME_TRACKER_REPO\docker\stop.cmd" 
  ```
  in your PowerShell.
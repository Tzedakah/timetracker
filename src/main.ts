import * as express from 'express'
import * as bodyParser from 'body-parser'

const app = express()
const PORT = process.env.PORT ? parseInt(process.env.PORT) : 8080

app.use(express.static('res'))
app.use(bodyParser.urlencoded({extended: true}))

app.post(
  '/event',
  (req, res) => {
    console.log(req.body)
    res.sendStatus(204)
  }
)

app.listen(PORT, () => console.log(`Listening on ${PORT}`))